/*!	\file SensorXtrSC.cpp
	\brief Library for managing the Xtreme sensor boards

	Copyright (C) 2019 Libelium Comunicaciones Distribuidas S.L.
	http://www.libelium.com

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 2.1 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.	If not, see <http://www.gnu.org/licenses/>.

	Version:		3.5
	Design:			David Gascón
	Implementation: Victor Boria, Javier Siscart

*/

#ifndef __WPROGRAM_H__
#include "WaspClasses.h"
#endif


#include "WaspSensorXtrSC.h"



/// Command table ////////////////////////////////////////////

const char string_00[] PROGMEM = "";
const char string_01[] PROGMEM = "";
const char string_02[] PROGMEM = "";
const char string_03[] PROGMEM = "";
const char string_04[] PROGMEM = "";
const char string_05[] PROGMEM = "";
const char string_06[] PROGMEM = "WARNING - Redefinition of sensor socket detected";
const char string_07[] PROGMEM = "WARNING - The following sensor can not work in the defined socket: ";
const char string_08[] PROGMEM = "";
const char string_09[] PROGMEM = "";
const char string_10[] PROGMEM = "socket (!): ";
const char string_11[] PROGMEM = "WARNING - Not possible to turn on two sensors at the same time: ";
const char string_12[] PROGMEM = "%c %s %s %c %s %s %s %s %s %s";							//GMX100 frame format
const char string_13[] PROGMEM = "%c %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX101 frame format
const char string_14[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX200 frame format
const char string_15[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %c %d %s %s %s %s %s %s"; //GMX240 frame format
const char string_16[] PROGMEM = "%c %s %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX300 frame format
const char string_17[] PROGMEM = "%c %s %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX301 frame format
const char string_18[] PROGMEM = "%c %s %s %s %d %s %s %s %s %s %c %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX400 frame format
const char string_19[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX500 frame format
const char string_20[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %d %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX501 frame format
const char string_21[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %c %d %s %s %s %d %s %s %s %d %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX531 GMX541 GMX551 frame format
const char string_22[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %s %s %c %d %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX550 GMX600frame format
const char string_23[] PROGMEM = ""; //not used
const char string_24[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX200 + GPS frame format
const char string_25[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %c %d %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX240 + GPS frame format
const char string_26[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX500 + GPS frame format
const char string_27[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX501 + GPS frame format
const char string_28[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %s %s %c %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX531 GMX541 GMX551 + GPS frame format
const char string_29[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %s %s %c %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX550 + GPS frame format
const char string_30[] PROGMEM = "%c %d %s %d %d %s %d %s %d %s %s %s %s %d %s %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %d %s %d %s %s"; //GMX600 + GPS frame format
const char string_31[] PROGMEM = "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //Eureka example frame format

const char* const table_xtr[] PROGMEM =
{
	string_00,
	string_01,
	string_02,
	string_03,
	string_04,
	string_05,
	string_06,
	string_07,
	string_08,
	string_09,
	string_10,
	string_11,
	string_12,
	string_13,
	string_14,
	string_15,
	string_16,
	string_17,
	string_18,
	string_19,
	string_20,
	string_21,
	string_22,
	string_23,
	string_24,
	string_25,
	string_26,
	string_27,
	string_28,
	string_29,
	string_30,
	string_31
};


//! class constructor
WaspSensorXtrSC::WaspSensorXtrSC()
{
	// Update Waspmote Control Register
	WaspRegisterSensor |= REG_XTR;

	// init variables
	// SW_XTR MUX -> done in main.cpp
	//pinMode(MUX_EN, OUTPUT);
	//digitalWrite(MUX_EN, HIGH);

	// pin configuration
	pinMode(MUX_A, OUTPUT);
	pinMode(MUX_B, OUTPUT);
	digitalWrite(MUX_A, LOW);
	digitalWrite(MUX_B, LOW);

	// 3v3 pins of adgs
	pinMode(_3V3_SOCKETA, OUTPUT);
	digitalWrite(_3V3_SOCKETA, LOW);
	pinMode(_3V3_SOCKETB, OUTPUT);
	digitalWrite(_3V3_SOCKETB, LOW);
	pinMode(_3V3_SOCKETD, OUTPUT);
	digitalWrite(_3V3_SOCKETD, LOW);
	pinMode(_3V3_SOCKETE, OUTPUT);
	digitalWrite(_3V3_SOCKETE, LOW);

	// DC DC
	pinMode(SW_12V, OUTPUT);
	digitalWrite(SW_12V, LOW);

	// I2C isolators
	pinMode(EXPAN_ISO_EN, OUTPUT);
	digitalWrite(EXPAN_ISO_EN, LOW);
	pinMode(I2C_SOCKETA_EN, OUTPUT);
	digitalWrite(I2C_SOCKETA_EN, LOW);
	pinMode(I2C_SOCKETD_EN, OUTPUT);
	digitalWrite(I2C_SOCKETD_EN, LOW);

	// SPI isolator
	pinMode(SPI_ISO_EN, OUTPUT);
	digitalWrite(SPI_ISO_EN, LOW);


	// State registers
	socketRegister = 0;
	stateRegister12v = 0;
	redefinedSocket = 0;

	bmeIsolatorEnabledSocketA = 0;
	bmeIsolatorEnabledSocketD = 0;
	luxesIsolatorEnabledSocketA = 0;
	luxesIsolatorEnabledSocketD = 0;
	ultrasoundIsolatorEnabledSocketA = 0;
	ultrasoundIsolatorEnabledSocketD = 0;
}


// Private Methods /////////////////////////////////////////////////////////////





// Public Methods //////////////////////////////////////////////////////////////

/*!
	\brief Turns ON the Xtreme board
	\param void
	\return void
*/
void WaspSensorXtrSC::ON()
{
	ON(REG_5V & REG_3V3);
}



/*!
	\brief Turns ON the Xtreme board
	\param power select between 5V, 3V3 or both
	\return void
*/
void WaspSensorXtrSC::ON(uint8_t power)
{
	switch (power)
	{
		case REG_5V:
			PWR.setSensorPower(SENS_5V, SENS_ON);
			break;

		case REG_3V3:
			PWR.setSensorPower(SENS_3V3, SENS_ON);
			break;

		case (REG_5V & REG_3V3):
			PWR.setSensorPower(SENS_3V3, SENS_ON);
			PWR.setSensorPower(SENS_5V, SENS_ON);
			break;

		default:
			break;
	}
	delay(10);
}

/*!
	\brief Turns OFF the Agriculture Xtreme board
	\param void
	\return void
*/
void WaspSensorXtrSC::OFF()
{
	//Only turn OFF 3V3 if every 12V register if OFF
	//3V3 is neccessary for 3v3 socket selecter pin expasor
	if (((WaspRegister & REG_3V3) != 0) && SensorXtrSC.stateRegister12v == 0)
	{
#if DEBUG_XTR == 2
		PRINTLN_XTR(F("3V3 OFF"));
#endif
		PWR.setSensorPower(SENS_3V3, SENS_OFF);
	}
	if ((WaspRegister & REG_5V) != 0)
	{
#if DEBUG_XTR == 2
		PRINTLN_XTR(F("5V OFF"));
#endif
		PWR.setSensorPower(SENS_5V, SENS_OFF);
	}

}



/*!
	\brief Manages the 3v3 power supplies of Agriculture Xtreme board
	\param socket socket to be powered
	\param state desired state of the selected power supply
	\return void
*/
void WaspSensorXtrSC::set3v3(uint8_t socket, uint8_t _state)
{

	if (_state == SWITCH_ON)
	{
		switch (socket)
		{
			case XTR_SOCKET_A:
				digitalWrite(_3V3_SOCKETA, HIGH);
				break;

			case XTR_SOCKET_B:
				digitalWrite(_3V3_SOCKETB, HIGH);
				break;

			case XTR_SOCKET_D:
				digitalWrite(_3V3_SOCKETD, HIGH);
				break;

			case XTR_SOCKET_E:
				digitalWrite(_3V3_SOCKETE, HIGH);
				break;

			default:
				break;
		}
	}
	else
	{
		switch (socket)
		{
			case XTR_SOCKET_A:
				digitalWrite(_3V3_SOCKETA, LOW);
				break;

			case XTR_SOCKET_B:
				digitalWrite(_3V3_SOCKETB, LOW);
				break;

			case XTR_SOCKET_D:
				digitalWrite(_3V3_SOCKETD, LOW);
				break;

			case XTR_SOCKET_E:
				digitalWrite(_3V3_SOCKETE, LOW);
				break;

			default:
				break;
		}
	}

}

/*!
	\brief Manages the 12v power supplies of Agriculture Xtreme board
	\param state desired state
	\return void
*/
void WaspSensorXtrSC::set12v(uint8_t socket, uint8_t _state)
{
	MCP23008 mcp;	// object to manage internal circuitry

	// enable I2C in pin expansor enabling isolator
	digitalWrite(EXPAN_ISO_EN, HIGH);
	delay(10);

	// enable DC-DC
	digitalWrite(SW_12V, HIGH);

	// Pin expander modes
	mcp.pinMode(XTR_SOCKET_A, OUTPUT);
	mcp.pinMode(XTR_SOCKET_B, OUTPUT);
	mcp.pinMode(XTR_SOCKET_C, OUTPUT);
	mcp.pinMode(XTR_SOCKET_D, OUTPUT);
	mcp.pinMode(XTR_SOCKET_E, OUTPUT);
	mcp.pinMode(XTR_SOCKET_F, OUTPUT);
	mcp.pinMode(MCP_GP0, INPUT);
	mcp.pinMode(MCP_GP1, INPUT);

	// update SensorXtrSC.stateRegister12v
	if (_state == SWITCH_ON)
	{
		//SensorXtrSC.stateRegister12v update
		bitSet(SensorXtrSC.stateRegister12v, socket);
	}
	else
	{
		//SensorXtrSC.stateRegister12v update
		bitClear(SensorXtrSC.stateRegister12v, socket);
	}

	// update every pin in expander
	for (int i = 2; i < 8; i++)
	{
		bool bit_state = bitRead(SensorXtrSC.stateRegister12v, i);
		mcp.digitalWrite(i, bit_state);
	}

	// if all 12v supplies are ON print warning to avoid overcurrents.
	if (SensorXtrSC.stateRegister12v >= 0b11111100)
	{
		PRINTLN_XTR(F("WARNING - Possible overcurrent, 12V is ON in all the sockets"));
	}

	//If every 12V register if OFF, then turn OFF DC-DC
	if (SensorXtrSC.stateRegister12v == 0)
	{
#if DEBUG_XTR == 2
		PRINTLN_XTR(F("12V OFF"));
#endif
		// disable DC-DC
		digitalWrite(SW_12V, LOW);
		delay(10);
	}

	// disable I2C in pin expansor disabling isolator
	digitalWrite(EXPAN_ISO_EN, LOW);
	delay(10);

}

/*!
	\brief Controls the on board multiplexor acording to socket selected
		OUT - socket - INPUT (A,B)
		Y0		- Data B -	(0,0)
		Y1		- Data C -	(1,0)
		Y2		- Data D -	(0,1)
		Y3		- Data A -	(1,1)
	\return void
*/
void WaspSensorXtrSC::setMux(uint8_t socket, uint8_t _state)
{
	if (_state == ENABLED)
	{
		// enable mux
		digitalWrite(MUX_EN, LOW);

		// set multiplexor according socket and turn on power if necessary
		switch (socket)
		{
			case XTR_SOCKET_A:
				digitalWrite(MUX_A, HIGH);
				digitalWrite(MUX_B, HIGH);
				break;

			case XTR_SOCKET_B:
				digitalWrite(MUX_A, LOW);
				digitalWrite(MUX_B, LOW);
				break;

			case XTR_SOCKET_C:
				digitalWrite(MUX_A, HIGH);
				digitalWrite(MUX_B, LOW);
				break;

			case XTR_SOCKET_D:
				digitalWrite(MUX_A, LOW);
				digitalWrite(MUX_B, HIGH);
				break;

			default:
				break;
		}
	}
	else
	{
		// disable mux
		digitalWrite(MUX_EN, HIGH);
	}

	delay(10);
}


//******************************************************************************
//ENVIROPRO4 Sensor Class functions
//******************************************************************************

/*!
	\brief ENVIROPRO4 Class constructor
	\param socket selected socket for sensor
*/
Entelechy_ENVIROPRO4::Entelechy_ENVIROPRO4(uint8_t _socket)
{
	// store sensor location
	socket = _socket;

	if (bitRead(SensorXtrSC.socketRegister, socket) == 1)
	{
		//Redefinition of socket by two sensors detected
		SensorXtrSC.redefinedSocket = 1;
	}
	else
	{
		bitSet(SensorXtrSC.socketRegister, socket);
	}
}

/*!
	\brief Turns on the sensor
	\param void
	\return 1 if ok, 0 if something fails
*/
uint8_t Entelechy_ENVIROPRO4::ON()
{
	char message[70];

	if (SensorXtrSC.redefinedSocket == 1)
	{
		#ifndef MANUFACTURER_TEST
		//"WARNING: Redefinition of sensor socket detected"
		strcpy_P(message, (char*)pgm_read_word(&(table_xtr[6])));
		PRINTLN_XTR(message);
		#endif
	}

	if ((socket == XTR_SOCKET_E) || (socket == XTR_SOCKET_F))
	{
		//"WARNING - The following sensor can not work in the defined socket:"
		strcpy_P(message, (char*)pgm_read_word(&(table_xtr[7])));
		PRINT_XTR(message);
		USB.println(F("TEROS11"));

		return 0;
	}

	//Before switching on 5v it's necessary disabling Mux (it works with 5V)
	SensorXtrSC.setMux(socket, DISABLED);

	SensorXtrSC.ON(); //SDI12 needs both 3v3 and 5v
	SensorXtrSC.set12v(socket, SWITCH_ON);

	//neccessary delay after powering the sensor
	delay(300);

#if DEBUG_XTR == 2
	//"socket (!): "
	strcpy_P(message, (char*)pgm_read_word(&(table_xtr[10])));
	PRINT_XTR(message);
	USB.println(socket, DEC);
#endif

	return 1;
}

/*!
	\brief Turns off the sensor
	\param void
	\return void
*/
void Entelechy_ENVIROPRO4::OFF()
{
	SensorXtrSC.set12v(socket, SWITCH_OFF);
	SensorXtrSC.OFF();

}

/*!
	\brief Reads the sensor data
	\param void
	\return 1 if ok, 0 if something fails
*/
uint8_t Entelechy_ENVIROPRO4::read()
{
	SensorXtrSC.setMux(socket, ENABLED);
	uint8_t counter = 0;
	uint8_t a = 0;
	uint8_t b = 0;
	uint8_t c = 0;
	uint8_t d = 0;
	uint8_t len = 0;
	char stringParameter1[20];
	char stringParameter2[20];
	char stringParameter3[20];
	char stringParameter4[20];
	uint8_t response = 0;
	uint8_t validMeasure = 0;
	uint8_t retries = 0;
	char measures[50];
	uint8_t i = 0;
	
	
//--------------------------------------------------------	
// READING moisture
//--------------------------------------------------------

	sdi12Sensor.sendCommand("0C3!", 4);
	//sdi12Sensor.readCommandAnswer(30, LISTEN_TIME);
	sdi12Sensor.readCommandAnswer(30, LISTEN_TIME_EP); //ND VARIATION

	// clear measures array
	memset(measures, 0x00, sizeof(measures));

	// store the reading in measures buffer
	i = 0;
	while (sdi12Sensor.available() && (i < 30))
	{
		measures[i] = sdi12Sensor.read();
		if (measures[i] == NULL) break;
		i++;
	}
	
	
	sdi12Sensor.sendCommand("0D0!", 4);
	sdi12Sensor.readCommandAnswer(30, 1000);

	// clear measures array
	memset(measures, 0x00, sizeof(measures));

	// store the reading in measures buffer
	i = 0;
	while (sdi12Sensor.available() && (i < 30))
	{
		measures[i] = sdi12Sensor.read();
		if (measures[i] == NULL) break;
		i++;
	}

// PARSING
	i = 1;  //ignore first value which is the address
	counter = 0;

	a = 0;
	b = 0;
	c = 0;
	d = 0;

	len = strlen(measures);

	stringParameter1[20];
	stringParameter2[20];
	stringParameter3[20];
	stringParameter4[20];

	//Empty the arrays
	memset(stringParameter1, 0x00, sizeof(stringParameter1));
	memset(stringParameter2, 0x00, sizeof(stringParameter2));
	memset(stringParameter3, 0x00, sizeof(stringParameter3));
	memset(stringParameter4, 0x00, sizeof(stringParameter4));

	//if invalid data, return 0.
	if (len == 0)
	{
			  USB.print("len  ");
			  USB.println(len);
		return 0;
	}

	while((counter <= 4) && (i <= len))
	{
		if ((measures[i] == '+') || (measures[i] == '-'))
		{
			counter++;
		}
		switch (counter)
		{
			case 1:
				stringParameter1[a] = measures[i];
				a++;
				break;

			case 2:
				stringParameter2[b] = measures[i];
				b++;
				break;

			case 3:
				stringParameter3[c] = measures[i];
				c++;
				break;

			case 4:
				stringParameter4[d] = measures[i];
				d++;
				break;

			default:
				break;
		}
		i++;
	}

	//add eof to strings
	stringParameter1[a] = '\0';
	stringParameter2[b] = '\0';
	stringParameter3[c] = '\0';
	stringParameter4[d] = '\0';

	// Convert strings to float values


	sensorENVIROPRO4.moisture1 = atof(stringParameter1);
	sensorENVIROPRO4.moisture2 = atof(stringParameter2);
	sensorENVIROPRO4.moisture3 = atof(stringParameter3);
	sensorENVIROPRO4.moisture4 = atof(stringParameter4);
//--------------------------------------------------------	
// READING temperature
//--------------------------------------------------------

	delay(500);
	sdi12Sensor.sendCommand("0C2!", 4);
//	sdi12Sensor.readCommandAnswer(30, LISTEN_TIME);
	sdi12Sensor.readCommandAnswer(30, LISTEN_TIME_EP); //ND VARIATION

	// clear measures array
	memset(measures, 0x00, sizeof(measures));

	// store the reading in measures buffer
	i = 0;
	while (sdi12Sensor.available() && (i < 30))
	{
		measures[i] = sdi12Sensor.read();
		if (measures[i] == NULL) break;
		i++;
	}
	
	
	sdi12Sensor.sendCommand("0D0!", 4);
	sdi12Sensor.readCommandAnswer(30, 1000);

	// clear measures array
	memset(measures, 0x00, sizeof(measures));

	// store the reading in measures buffer
	i = 0;
	while (sdi12Sensor.available() && (i < 30))
	{
		measures[i] = sdi12Sensor.read();
		if (measures[i] == NULL) break;
		i++;
	}
	sdi12Sensor.setState(DISABLED);
	SensorXtrSC.setMux(socket, DISABLED);


// PARSING
	i = 1;  //ignore first value which is the address
	counter = 0;

	a = 0;
	b = 0;
	c = 0;
	d = 0;

	len = strlen(measures);

	stringParameter1[20];
	stringParameter2[20];
	stringParameter3[20];
	stringParameter4[20];

	//Empty the arrays
	memset(stringParameter1, 0x00, sizeof(stringParameter1));
	memset(stringParameter2, 0x00, sizeof(stringParameter2));
	memset(stringParameter3, 0x00, sizeof(stringParameter3));
	memset(stringParameter4, 0x00, sizeof(stringParameter4));

	//if invalid data, return 0.
	if (len == 0)
	{
			  USB.print("len  ");
			  USB.println(len);
		return 0;
	}

	while((counter <= 4) && (i <= len))
	{
		if ((measures[i] == '+') || (measures[i] == '-'))
		{
			counter++;
		}
		switch (counter)
		{
			case 1:
				stringParameter1[a] = measures[i];
				a++;
				break;

			case 2:
				stringParameter2[b] = measures[i];
				b++;
				break;

			case 3:
				stringParameter3[c] = measures[i];
				c++;
				break;

			case 4:
				stringParameter4[d] = measures[i];
				d++;
				break;

			default:
				break;
		}
		i++;
	}

	//add eof to strings
	stringParameter1[a] = '\0';
	stringParameter2[b] = '\0';
	stringParameter3[c] = '\0';
	stringParameter4[d] = '\0';

	// Convert strings to float values


	sensorENVIROPRO4.temperature1 = atof(stringParameter1);
	sensorENVIROPRO4.temperature2 = atof(stringParameter2);
	sensorENVIROPRO4.temperature3 = atof(stringParameter3);
	sensorENVIROPRO4.temperature4 = atof(stringParameter4);
	
	return 1;
}

/*!
	\brief Reads the sensor serial number
	\param void
	\return 1 if ok, 0 if something fails
*/
uint8_t Entelechy_ENVIROPRO4::readSerialNumber()
{
	return read();
}

// Preinstantiate Objects //////////////////////////////////////////////////////

WaspSensorXtrSC SensorXtrSC = WaspSensorXtrSC();