/*! \file WaspSensorXtrSC.h
	\brief Library for managing the Xtreme sensor boards

	Copyright (C) 2019 Libelium Comunicaciones Distribuidas S.L.
	http://www.libelium.com

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 2.1 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.	If not, see <http://www.gnu.org/licenses/>.

	Version:		3.5
	Design:			David Gascón
	Implementation: Victor Boria, Javier Siscart

 */

#ifndef WaspSensorXtrSC_h
#define WaspSensorXtrSC_h

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <inttypes.h>
#include "..\SensorXtr\utility/MCP23008.h"
//#include "./utility/ADC.h"
#include <BME280.h>
#include <UltrasoundSensor.h>
#include <TSL2561.h>
#include <SDI12.h>
//#include <./utility/AqualaboModbusSensors.h>
#include "ModbusMaster.h"
#include <WaspUtils.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*!
 * \def DEBUG_XTR
 * \brief Possible values:
 * 	0: No debug mode enabled
 * 	1: debug mode enabled for error output messages
 * 	2: debug mode enabled for both error and ok messages
 */

#define DEBUG_XTR			0
#define PRINT_XTR(str)		USB.print(F("[XTR] ")); USB.print(str);
#define PRINTLN_XTR(str)	USB.print(F("[XTR] ")); USB.println(str);

#define SIMULATE_EUREKA				0
#define SIMULATE_WEATHER_STATION	0

//#define MANUFACTURER_TEST

// Generic _socket definitions according to MCP GPIOs
#define XTR_SOCKET_A		7
#define XTR_SOCKET_B		6
#define XTR_SOCKET_C		5
#define XTR_SOCKET_D		4
#define XTR_SOCKET_E		3
#define XTR_SOCKET_F		2

#define MCP_GP1				1		// Unconneted MCP GPIO
#define MCP_GP0				0		// Unconneted MCP GPIO


// PIN Expander definitions
#define EXPAN_ISO_EN 		17 		// ANALOG4 pin used as Digital
#define I2C_SOCKETA_EN		2		// DIGITAL1
#define I2C_SOCKETD_EN		14		// ANALOG1
#define SPI_ISO_EN			3		// DIGITAL0

#define SW_12V				8		// DIGITAL2


// 3V3 power on _sockets
#define _3V3_SOCKETA 		19
#define _3V3_SOCKETB 		18
#define _3V3_SOCKETD 		15
#define _3V3_SOCKETE		5


// SDI-12 mux
#define MUX_A				7		// DIGITAL5
#define MUX_B				9		// DIGITAL4
#define MUX_EN				20 		// ANA6


#define SWITCH_ON 			1
#define SWITCH_OFF 			0

#define ENABLED				1
#define DISABLED 			0


// ADC channel definition.
#define ADC_CH0				0		// 4-20 mA	(B)
#define ADC_CH1				1		// 4-20 mA	(F)
#define ADC_CH2				2		// RAD		(F)
#define ADC_CH3				3		// LEAF		(B)
#define ADC_CH4				4		// RAD		(E)
#define ADC_CH5				5		// DENDRO	(C)

// Dendrometer types supported (value = range in mm)
#define DENDRO_DD			11
#define DENDRO_DF			15
#define DENDRO_DC3			25


// Weather station models
#define WS_GMX100			1
#define WS_GMX101			2
#define WS_GMX200			3
#define WS_GMX240			4
#define WS_GMX300			5
#define WS_GMX301			6
#define WS_GMX400			7
#define WS_GMX500			8
#define WS_GMX501			9
#define WS_GMX531			10
#define WS_GMX541			11
#define WS_GMX550			12
#define WS_GMX551			13
#define WS_GMX600			14

//Parameters
#define PARAMETER_1 	1
#define PARAMETER_2 	2
#define PARAMETER_3		3
#define PARAMETER_4		4
#define TEMPERATURE		5


#define COMPENSATES_1		1
#define COMPENSATES_2		2
#define COMPENSATES_3		3
#define COMPENSATES_TEMP	5


// Supported sensors with Xtreme boards
#define _5TE				1
#define GS3					2
#define VP4					3
#define MPS6				4
#define SO411				5
#define SI411				6
#define _5TM				7
#define SF421				8
#define C4E					9
#define CTZN				10
#define MES5				11
#define OPTOD				12
#define PHEHT				13
#define NTU					14
#define TEROS12				15
#define TEROS11				16
#define SAC					17
#define ENVIROPRO4			18


#define EUREKA_PH			0
#define EUREKA_ORP			1
#define EUREKA_DPTH			2
#define EUREKA_COND			3
#define EUREKA_HDO			4
#define EUREKA_CHL			5
#define EUREKA_NH4			6
#define EUREKA_NO3			7
#define EUREKA_CL			8
#define EUREKA_BG			9
#define EUREKA_TURB			10


#define C21_LOW_POWER_MODE 0
#define C21_NORMAL_POWER_MODE 1
#define C21_DISTANCE_IN_M 0
#define C21_DISTANCE_IN_FT 1
#define C21_TEMPERATURE_IN_C 0
#define C21_TEMPERATURE_IN_F 1




/*******************************************************************************
 * Smart Agriculture Xtreme Classes
 ******************************************************************************/

/*!
 * \class WaspSensorXtrSC
 * \brief class for Xtreme boards
 */
class WaspSensorXtrSC
{
	public:
		//! Constructor
		WaspSensorXtrSC();

		/*
		* Bit:		| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
		* _socket: 	| A | B | C | D | E | F | - | - |
		*/
		uint8_t socketRegister;					//variable to store information about _sockets used by sensors
		uint8_t stateRegister12v;				//variable to store information about _sockets with 12v enabled
		bool redefinedSocket;					//flag to detect redefinitions of _socket by more than one sensor
		bool bmeIsolatorEnabledSocketA;			//flag to detect two bme sensors connected at the same time to i2c
		bool bmeIsolatorEnabledSocketD;			//flag to detect two bme sensors connected at the same time to i2c
		bool luxesIsolatorEnabledSocketA;		//flag to detect two luxes sensors connected at the same time to i2c
		bool luxesIsolatorEnabledSocketD;		//flag to detect two luxes sensors connected at the same time to i2c
		bool ultrasoundIsolatorEnabledSocketA;	//flag to detect two ultrasound sensors connected at the same time to i2c
		bool ultrasoundIsolatorEnabledSocketD;	//flag to detect two ultrasound sensors connected at the same time to i2c
		uint32_t readBoardSerialNumber();
		uint8_t writeEEPROM(uint8_t address, uint8_t value);
		int8_t readEEPROM(uint8_t address);
		uint8_t boardSerialNumber[4];			//Serial number in Xtreme board

	 	void ON();
	 	void ON(uint8_t power);
	 	void OFF();
	 	void set3v3(uint8_t _socket, uint8_t _state);
	 	void set12v(uint8_t _socket, uint8_t _state);
	 	void setMux(uint8_t _socket, uint8_t _state);

	private:


};

extern WaspSensorXtrSC SensorXtrSC;


/*******************************************************************************
 * Smart Agriculture Xtreme Structs  Simplycity sensors
 ******************************************************************************/

/*!
 * \struct sensorENVIRPRO4Vector
 * \brief Struct to store data of the ENVIROPRO4 sensor
 */

struct sensorENVIRPRO4Vector
{

  //! Variable: stores 4 moisture measurements in float type
  float moisture1;
  float moisture2;
  float moisture3;
  float moisture4;
  
//! Variable: stores measured temperature in degrees Celsius in float type
 
  float temperature1;
  float temperature2;
  float temperature3;
  float temperature4;
  
	//Sensor serial number variable
	char sensorSerialNumber[14];
};


/*!
 * \class Entelechy_ENVIROPRO4
 * \brief class for ENVIROPRO4 sensor
 */
class Entelechy_ENVIROPRO4
{
	public:
		// constructor
		Entelechy_ENVIROPRO4(uint8_t _socket);

		sensorENVIRPRO4Vector sensorENVIROPRO4;

		uint8_t ON();
		void OFF();
		uint8_t read();
		uint8_t readSerialNumber();
		WaspSDI12 sdi12Sensor = WaspSDI12(ANA2);

	private:
		uint8_t socket;
};

#endif