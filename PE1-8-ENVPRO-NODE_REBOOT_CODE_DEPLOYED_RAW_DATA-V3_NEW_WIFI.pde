
/*
    ###  Libelium Waspmote Plug&Sense Smart Agriculture Xtreme WiFi node transmitting Data to SIMPLYCITY ###

    Explanation: This program configures  the Libelium Waspmote Plug&Sense Smart Agriculture Xtreme WiFi node to send data to SimplyCity over WiFi
    Sensors Connected: BME (Temperature, Humidity, Pressure), Soil Oxygen Level, Soil Temeprature & Volumetric Water Content
    This program is developed by SIMPLYCITY, for Pureearth Pty Ltd.

*/

/*
   Device Info:
   ------------
   Device Name: PE_SAX_WI_01_PE2
   Device Model: Plug & Sense Smart Agriculture Xtreme
   Serial ID: 205B44E819623C1D
*/

// Libraries used
#include <WaspSensorXtrSC.h>
#include <WaspSensorXtr.h>
#include <WaspWIFI_PRO_V3.h>
#include <WaspRTC.h>


//Device Asset Info
//char DEVICE[] = "PE_SAX_WI_01_PE";
//char ASSET[] = " Device Name: PE_SAX_WI_01_PE2 \n Device Model: Plug & Sense Smart Agriculture Xtreme WiFi \n Serial ID: 205B44E819623C1D";

//WiFi Connection variables
uint8_t socket = SOCKET0;
char SSID[] = "Automation"; //Wottating WIFI
char PASSW[] = "V5KB5RJ927";
//char SSID[] = "Domingo"; //ND tet Wifi
//char PASSW[] = "Geronimo778!";
//char SSID[] = "SimplyCity-Tech"; //SimplyCity Test Wifi
//char PASSW[] = "TechSimplyCity"; 

//PURE EARTH Connection variables
char HTTP_SERVER[]= "192.168.0.12";
//char HTTP_PATH[] = "pe"; //used in V2 POST
//char payload[250];
//uint16_t HTTP_PORT = 1880;

//SIMPLYCITY Connection variables
//char type[] = "http";
//char HTTP_SERVER[] = "10.1.1.83";
//char HTTP_SERVER[] = "103.1.186.125";
char HTTP_PATH[250] ; //USED FOR GET METHOD. The actual path has to be built into the data string, see line 413

//char payload[200];
//char HTTP_DATA[250];
uint16_t HTTP_PORT = 1880;

//Error handling variables
uint8_t error;
uint8_t status;
unsigned long previous;
uint16_t socket_handle = 0;

// Variable to store the read value
//float vwlc;
//float rawperc;
//char dev_id[] = "pe1";
//char dev_id[] = "pe2";
//char dev_id[] = "pe3";
//char dev_id[] = "pe4";
//char dev_id[] = "pe5";
//char dev_id[] = "pe6";
//char dev_id[] = "pe7";
char dev_id[] = "pe8";

char sco[8];
char sbt[8];
char atc[8];
char reh[8];
char prs[8];
//char epc[12];
char bat[8];
char s_moisture_1[8];
char s_moisture_2[8];
char s_moisture_3[8];
char s_moisture_4[8];
char s_temp_1[8];
char s_temp_2[8];
char s_temp_3[8];
char s_temp_4[8];

//Enviropro 4 variables
char aux[4];
char command[6];
char serial;
int result;
float moisture_1;
float moisture_2;
float moisture_3;
float moisture_4;
float soil_temp_1;
float soil_temp_2;
float soil_temp_3;
float soil_temp_4;

//Instance objects
bme s_bme (XTR_SOCKET_A);
Apogee_SO421 s_sox(XTR_SOCKET_B);
Entelechy_ENVIROPRO4 soilMoisture(XTR_SOCKET_C);

void setup()
{

  USB.ON();
  USB.println(F("----------------------------------------------"));
  USB.print(F("The current time is: "));
  USB.print(RTC.getTime());
//  USB.println(F(": DEVICE INFO"));
//  USB.println(F("----------------------------------------------"));
//  USB.println(ASSET);
//  USB.println(F("\n"));
  USB.println(F("----------------------------------------------"));
  USB.println(F(": INTIAL SETUP MODE"));
  USB.println(F("----------------------------------------------"));

  ////////////////////////////////////////////////////////////////////
  /// 1. Set Alarm 2 to wake up the system every day at 3 AM
  ////////////////////////////////////////////////////////////////////
  USB.println("1. Setting Alarm 2 for reboot");
  RTC.setAlarm2(0, 03, 00, RTC_ABSOLUTE, RTC_ALM2_MODE3); //Use absolute time for exact time of day
  USB.println("Alarm Set for reboot");
  USB.print(F("Alarm2: "));
  USB.println(RTC.getAlarm2());
  //////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////
  // 2. Switch ON the WiFi module
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.ON(socket);

  if (error == 0)
  {
    USB.println(F("2. WiFi switched ON"));
  }
  else
  {
    USB.println(F("2. WiFi did not initialize correctly"));
  }


  //////////////////////////////////////////////////
  // 3. Reset to default values
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.resetValues();

  if (error == 0)
  {
    USB.println(F("3. WiFi reset to default"));
  }
  else
  {
    USB.println(F("3. WiFi reset to default ERROR"));
  }

  //////////////////////////////////////////////////
  // 3. Set unit as client for an AP
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.configureMode(WaspWIFI_v3::MODE_STATION);

  if (error == 0)
  {
    USB.println(F("3. WiFi configured OK"));
  }
  else
  {
    USB.println(F("3. WiFi configured ERROR"));
  }
  //////////////////////////////////////////////////
  // 4. Configure SSID, Password and Autoconnect
  //////////////////////////////////////////////////
  //////////////////////////////////////////////////
  // 4. Configure SSID and password and autoconnect
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.configureStation(SSID, PASSW, WaspWIFI_v3::AUTOCONNECT_ENABLED);

  if (error == 0)
  {
    USB.println(F("4. WiFi configured SSID OK"));
  }
  else
  {
    USB.print(F("4. WiFi configured SSID error: "));
    USB.println(error, DEC);
  }
  if (error == 0)
  {
    USB.println(F("5. WiFi connected to AP OK"));

    USB.print(F("SSID: "));
    USB.println(WIFI_PRO_V3._essid);

    USB.print(F("Channel: "));
    USB.println(WIFI_PRO_V3._channel, DEC);

    USB.print(F("Signal strength: "));
    USB.print(WIFI_PRO_V3._power, DEC);
    USB.println("dB");

    USB.print(F("IP address: "));
    USB.println(WIFI_PRO_V3._ip);

    USB.print(F("GW address: "));
    USB.println(WIFI_PRO_V3._gw);

    USB.print(F("Netmask address: "));
    USB.println(WIFI_PRO_V3._netmask);

    WIFI_PRO_V3.getMAC();

    USB.print(F("MAC address: "));
    USB.println(WIFI_PRO_V3._mac);
  }
  else
  {
    USB.print(F("5. WiFi connect error: "));
    USB.println(error, DEC);

    USB.print(F("Disconnect status: "));
    USB.println(WIFI_PRO_V3._status, DEC);

    USB.print(F("Disconnect reason: "));
    USB.println(WIFI_PRO_V3._reason, DEC);


    //////////////////////////////////////////////////
    // 3. Configure HTTP conection
    //////////////////////////////////////////////////

    //    error = WIFI_PRO_V3.httpConfiguration(HTTP_SERVER, HTTP_PORT);
    //    if (error == 0)
    //    {
    //      USB.println(F("3. HTTP conection configured"));
    //    }
    //    else
    //    {
    //      USB.print(F("3. HTTP conection configured ERROR"));
    //    }

  }
  //////////////////////////////////////////////////
  // 6. Software Reset
  // Parameters take effect following either a
  // hardware or software reset
  //////////////////////////////////////////////////
  //  error = WIFI_PRO_V3.softReset();
  //
  //  if (error == 0)
  //  {
  //    USB.println(F("6. WiFi softReset OK"));
  //  }
  //  else
  //  {
  //    USB.println(F("6. WiFi softReset ERROR"));
  //  }


  USB.println(F("*******************************************"));
  USB.println(F("Once the module is configured with ESSID"));
  USB.println(F("and PASSWORD, the module will attempt to "));
  USB.println(F("join the specified Access Point on power up"));
  USB.println(F("*******************************************\n"));

  // get current time
  previous = millis();
}
void loop()
{
  RTC.ON();
  USB.ON();
  USB.println(F("----------------------------------------------"));
  USB.print(RTC.getTime());
  USB.println(F(": LOOP OPERATION MODE"));
  USB.println(F("----------------------------------------------"));
  USB.println(F("1. Sensor data collection - START"));


  //Read the Soil Oxygen sensor
  USB.println("Reading Soil Oxygen Sensor - START");
  s_sox.ON();
  //UNCOMMENT THE DELAY LINE BELOW FOR PRODUCTION DEPLOYMENT
  //delay(60000);
  s_sox.read();
  dtostrf(s_sox.sensorSO421.calibratedOxygen, 1, 2, sco);
  USB.printFloat(s_sox.sensorSO411.calibratedOxygen, 3);
  dtostrf(s_sox.sensorSO411.bodyTemperature, 1, 2, sbt);
  USB.println();
  USB.printFloat(s_sox.sensorSO421.bodyTemperature, 1);
  USB.println();
  USB.printFloat(s_sox.sensorSO421.milliVolts, 4);
  s_sox.OFF();
  USB.println();
  USB.println("Reading Soil Oxygen Sensor - DONE");
  //Read the first air sensor
  USB.println("Reading Air Sensor - START");
  s_bme.ON();
  dtostrf(s_bme.getTemperature(), 1, 2, atc);
  dtostrf(s_bme.getHumidity(), 1, 2, reh);
  dtostrf(s_bme.getPressure() / 1000, 1, 2, prs);
  s_bme.OFF();
  USB.println("Reading Air Sensor - DONE");

//  USB.println("Readings Soil Moisture - START");
  soilMoisture.ON();
  result =    soilMoisture.read();

  moisture_1 = soilMoisture.sensorENVIROPRO4.moisture1;
  moisture_2 = soilMoisture.sensorENVIROPRO4.moisture2;
  moisture_3 = soilMoisture.sensorENVIROPRO4.moisture3;
  moisture_4 = soilMoisture.sensorENVIROPRO4.moisture4;
  soil_temp_1 = soilMoisture.sensorENVIROPRO4.temperature1;
  soil_temp_2 = soilMoisture.sensorENVIROPRO4.temperature2;
  soil_temp_3 = soilMoisture.sensorENVIROPRO4.temperature3;
  soil_temp_4 = soilMoisture.sensorENVIROPRO4.temperature4;

  //Converting the readings in preparation for the payload
  dtostrf(soilMoisture.sensorENVIROPRO4.moisture1, 1, 2, s_moisture_1);
  USB.print("s_moisture_1 result is :");
  USB.println(s_moisture_1);
  delay(3000);
  dtostrf(soilMoisture.sensorENVIROPRO4.moisture2, 1, 2, s_moisture_2);
  dtostrf(soilMoisture.sensorENVIROPRO4.moisture3, 1, 2, s_moisture_3);
  dtostrf(soilMoisture.sensorENVIROPRO4.moisture4, 1, 2, s_moisture_4);

  dtostrf(soilMoisture.sensorENVIROPRO4.temperature1, 1, 2, s_temp_1);
  dtostrf(soilMoisture.sensorENVIROPRO4.temperature2, 1, 2, s_temp_2);
  dtostrf(soilMoisture.sensorENVIROPRO4.temperature3, 1, 2, s_temp_3);
  dtostrf(soilMoisture.sensorENVIROPRO4.temperature4, 1, 2, s_temp_4);
  USB.print("s_temp_4 result is :");
  USB.println(s_temp_4);
  delay(3000);



  USB.println(F("EnviroPro4"));
  USB.print(F("Data from Sensor "));

  USB.print(F("moisture 1="));
  USB.println(soilMoisture.sensorENVIROPRO4.moisture1);
  USB.println(moisture_1);
  USB.print(F("moisture 2="));
  USB.println(soilMoisture.sensorENVIROPRO4.moisture2);
  USB.println(moisture_2);
  USB.print(F("moisture 3="));
  USB.println(soilMoisture.sensorENVIROPRO4.moisture3);
  USB.println(moisture_3);
  USB.print(F("moisture 4="));
  USB.println(soilMoisture.sensorENVIROPRO4.moisture4);
  USB.println(moisture_4);

  USB.print(F("temperature 1="));
  USB.println(soilMoisture.sensorENVIROPRO4.temperature1);
  USB.println(soil_temp_1);
  USB.print(F("temperature 2="));
  USB.println(soilMoisture.sensorENVIROPRO4.temperature2);
  USB.println(soil_temp_2);
  USB.print(F("temperature 3="));
  USB.println(soilMoisture.sensorENVIROPRO4.temperature3);
  USB.println(soil_temp_3);
  USB.print(F("temperature 4="));
  USB.println(soilMoisture.sensorENVIROPRO4.temperature4);
  USB.println(soil_temp_4);

  USB.println(F("---------------------------\n"));

  //Read timestamp
  // dtostrf(RTC.getEpochTime(), 1, 0, epc);
  dtostrf(PWR.getBatteryLevel(), 1, 2, bat);

  //  USB.println(F("2. Sensor data collection - COMPLETE - Here is the Data"));
  //  USB.print(F("     Timestamp(epoch): "));
  //  USB.println(epc);
  USB.print(F("     Soil Oxygen (Calibrated): "));
  USB.print(sco);
  USB.println(F("%"));
  USB.print(F("     Body Temperature: "));
  USB.print(sbt);
  USB.println(F("°C"));
  USB.print(F("     Air Temperature: "));
  USB.print(atc);
  USB.println(F("°C"));
  USB.print(F("     Air Humidity: "));
  USB.print(reh);
  USB.println(F("%"));
  USB.print(F("     Air pressure: "));
  USB.print(prs);
  USB.println(F("kPa"));
  //  USB.print(F("     Soil Temperature: "));
  //  USB.print(stc);
  USB.println(F("°C"));
  USB.print(F("     Battery Level: "));
  USB.print(bat);
  USB.println(F("%"));

  //Prepare Data frame to transmit

  //  snprintf(payload, sizeof(payload), "dev_id=%s&sco=%s&sbt=%s&atc=%s&reh=%s&prs=%s&s_moisture_1=%s&s_moisture_2=%s&s_moisture_3=%s&s_moisture_4=%s&s_temp_1=%s&s_temp_2=%s&s_temp_3=%s&s_temp_4=%s&bat=%s", dev_id, sco, sbt, atc, reh, prs, s_moisture_1, s_moisture_2, s_moisture_3, s_moisture_4, s_temp_1, s_temp_2, s_temp_3, s_temp_4, bat);
  //  USB.print(F("Sensor Data Payload:\n"));
  //  // USB.println(HTTP_DATA);
  //  USB.println(payload);

//  snprintf(HTTP_DATA, sizeof(HTTP_DATA), "dev_id=%s&sco=%s&sbt=%s&atc=%s&reh=%s&prs=%s&s_moisture_1=%s&s_moisture_2=%s&s_moisture_3=%s&s_moisture_4=%s&s_temp_1=%s&s_temp_2=%s&s_temp_3=%s&s_temp_4=%s&bat=%s", dev_id, sco, sbt, atc, reh, prs, s_moisture_1, s_moisture_2, s_moisture_3, s_moisture_4, s_temp_1, s_temp_2, s_temp_3, s_temp_4, bat);
//  USB.print(F("Sensor Data Payload:\n"));
//  USB.println(HTTP_DATA);

  //The following works but it is a GET method
  snprintf(HTTP_PATH, sizeof(HTTP_PATH), "/pe_get?dev_id=%s&sco=%s&sbt=%s&atc=%s&reh=%s&prs=%s&s_moisture_1=%s&s_moisture_2=%s&s_moisture_3=%s&s_moisture_4=%s&s_temp_1=%s&s_temp_2=%s&s_temp_3=%s&s_temp_4=%s&bat=%s", dev_id, sco, sbt, atc, reh, prs, s_moisture_1, s_moisture_2, s_moisture_3, s_moisture_4, s_temp_1, s_temp_2, s_temp_3, s_temp_4, bat);
    USB.print(F("Sensor Data Payload:\n"));
   USB.println(HTTP_DATA);
    USB.println(HTTP_PATH);
  //////////////////////////////////////////////////
  // 1. Switch ON WI-FI
  //////////////////////////////////////////////////
  error = WIFI_PRO_V3.ON(socket);

  if (error == 0)
  {
    USB.println(F("1. WiFi switched ON"));
  }
  else
  {
    USB.println(F("1. WiFi did not initialize correctly"));
  }

  //////////////////////////////////////////////////
  // 3. Join AP
  //////////////////////////////////////////////////

  // check connectivity
  status =  WIFI_PRO_V3.isConnected();

  // check if module is connected
  if (status == true)
  {
    USB.println(F("2. WiFi is connected OK"));

    USB.print(F("IP address: "));
    USB.println(WIFI_PRO_V3._ip);

    USB.print(F("GW address: "));
    USB.println(WIFI_PRO_V3._gw);

    USB.print(F("Netmask address: "));
    USB.println(WIFI_PRO_V3._netmask);

    USB.print(F(" Time(ms):"));
    USB.println(millis() - previous);

    //////////////////////////////////////////////////
    // 1. Perform the HTTP GET
    //////////////////////////////////////////////////
    WIFI_PRO_V3.httpConfiguration(HTTP_SERVER, HTTP_PORT);
    //the folllowing line is a test for http data
//    char HTTP_DATA[]="/+test";
//    char HTTP_PATH[]="/scpe";
//     error = WIFI_PRO_V3.httpPost(HTTP_PATH,HTTP_DATA);
    error = WIFI_PRO_V3.httpGet(HTTP_PATH);

    // check response
    if (error == 0)
    {
      USB.print(F("HTTP POST done!\r\nStatus: "));
      USB.println(WIFI_PRO_V3._httpResponseStatus, DEC);
      USB.print(F("\nServer answer:"));
      USB.println(WIFI_PRO_V3._buffer, WIFI_PRO_V3._bufferSize);
    }
    else
    {
      USB.println(F("Error in HTTP POST!"));
      USB.println(WIFI_PRO_V3._httpResponseStatus, DEC);
    }
  }
  else
  {
    USB.print(F("3. WiFi is connected ERROR"));
    USB.print(F(" Time(ms):"));
    USB.println(millis() - previous);
  }


  //////////////////////////////////////////////////
  // 4. Switch OFF
  //////////////////////////////////////////////////
  WIFI_PRO_V3.OFF(socket);
  USB.println(F("4. WiFi switched OFF\n"));
  //  delay(20000);
  //}


  //  frame.createFrame(ASCII);// creating a Smart Agriculture Xtreme frame
  //  frame.showFrame();
  //  USB.println(F("4. Data Frame Construction - COMPLETE"));
  //
  //  //Send data frame over WiFi
  //  USB.println(F("5. Data Frame Transmission - START"));
  //  if (error == 0){
  //    USB.println(F("6. Data Frame Transmission - SUCCESS"));
  //  }
  //  else
  //  {
  //    USB.println(F("6. Data Frame Transmission - FAILED"));
  //    USB.print(F("     Error Code: "));
  //    USB.println(error);
  //  }
  //

  //////////////////////////////////////////////////////////////////////////////
  ///The Following checks interrupt flags and performs the Watchdog reset
  ///////////////////////////////////////////////////////////////////////////////
  //  USB.println("5. Checking interrupt flag for reboot");
  //  if ( intFlag & RTC_INT ) //checks if there is an interrupt flag
  //  {
  //    if (RTC.alarmTriggered == 2) //checks to see if the interrupt flag has been triggered by Alarm 2. Otherwise it will disregard the flag and go to normal sleep mode
  //    {
  //      intFlag &= ~(RTC_INT); // Clear flag
  //      clearIntFlag(); //another clear of the flag
  //      USB.println("Interrupt detected, rebooting in 1 minute");
  //      RTC.setWatchdog(1); //reboots te unit withina  minute from the moment it is called
  //      delay(70000); //70 seconds delay to allow for watchdog reset
  //    }
  //  }


  USB.println("6. Entering hibernation, wake up in 15 mins");
  USB.println(F("----------------------------------------------\n"));
  //UNCOMMENT DEEP SLEEP LINE BELOW FOR PRODUCTION DEPLOYMENT
  PWR.deepSleep("00:00:15:00", RTC_OFFSET, RTC_ALM1_MODE1, ALL_OFF); //uses OFFSET to set the 15 minute interval from the last transmission

  //COMMENT DELAY LINE BELOW FOR PRODUCTION DEPLOYMENT
  //delay(10000);
}



